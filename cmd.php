<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once 'db.php';

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');

$db = new database();
$cmd = key_exists('cmd', $_POST) ? $_POST['cmd'] : $_GET['cmd'];
$result = (object)[];
$currentUser = null;

function _dbError(){
    global $db, $result;
    
    $db->error = str_replace("\n", "; ", $db->error);
    header("HTTP/1.1 400 $db->error", true);
}

function _getIP() {
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

function _generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function _createSession($user){
    session_start();
    session_regenerate_id(true);
    
    $time = new DateTime("now");
    
    $_SESSION['user'] = $user;
    $_SESSION['time'] = $time->getTimestamp();
    $_SESSION['ip'] = _getIP();
    $_SESSION['user_agent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
    
    return session_id();
    
    
    
//    $time = new DateTime("now");
//    $session = _generateRandomString();
//    if(!file_exists("/tmp/session")){
//        mkdir("/tmp/session", 0777);
//    }
//    file_put_contents("/tmp/session/$session", json_encode((object)[
//        "user" => $user,
//        "time" => $time->getTimestamp(),
//        "ip" => _getIP()
//    ]));
//    
//    return $session;
}

function _checkSession($session){
    if($session !== ""){
        session_id($session);
    }
    session_start();
    
    if(!isset($_SESSION) || !isset($_SESSION['user']) || !isset($_SESSION['time']) || !isset($_SESSION['ip']) || !isset($_SESSION['user_agent'])){
        return false;
    }
    $now = new DateTime("now");
    $time = $_SESSION['time'];
    $ip = $_SESSION['ip'];
    $user_agent = $_SESSION['user_agent'];
    $req_user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
    
    if($ip != _getIP() || $req_user_agent != $user_agent || ($now->getTimestamp() - $time) > 3600){
        session_destroy();
        return false;
    }
    
    $_SESSION['time'] = $now->getTimestamp();
    
    return $_SESSION['user'];
    
    
//    if($session === "")
//        return false;
//    
//    $file = "/tmp/session/$session";
//    
//    if(!file_exists($file))
//        return false;
//    
//    $data = file_get_contents($file);
//    if(!$data)
//        return false;
//    
//    $sessObj = json_decode($data);
//    $now = new DateTime("now");
//    $time = $sessObj->time;
//    
//    if($sessObj->ip != _getIP() || ($now->getTimestamp() - $time) > 3600){
//        unlink($file);
//        return false;
//    }
//    
//    $sessObj->time = $now->getTimestamp();
//    file_put_contents($file, json_encode($sessObj));
//    
//    return $sessObj->user;
}

function _clearSessions() {
//    if(!file_exists("/tmp/session"))
//        return;
//    
//    system("find /tmp/session/* -mmin +60 -exec rm {} \\;");
}


if($cmd != 'checkUser'){
    $session = key_exists('session', $_POST) ? $_POST['session'] : "";
    if($session === ""){
        $session = key_exists('session', $_GET) ? $_GET['session'] : "";
    }

    $currentUser = _checkSession($session);
    if(!$currentUser){
        header("HTTP/1.1 401 Unauthorized");
        exit();
    }
}

try{
    switch($cmd){
        case 'checkUser':
            _clearSessions();
            $name = isset($_POST['name']) ? $_POST['name'] : "noname";
            $pass = isset($_POST['pass']) ? $_POST['pass'] : "nopass";
            $user = $db->checkUser($name, $pass);
            if($user){
                if($user->Id != -1){
                    $session = _createSession($user);
                    $user->session = $session;
                }
                $result = $user;
            }else{
                _dbError();
            }
            break;

        case 'test':
    //        echo system("find /tmp/session/* -mmin +60 -exec rm {} \\;");
            $result = $_SESSION;
            break;

        case 'logout':
            session_destroy();
            $result = $_SESSION;
            break;

        case "projectList":
            $list = $db->projectList();
            if($list){
                $result = $list;
            }else{
                _dbError();
            }
            break;

        case "objectList":
            $userId = $_POST['userId'];
            $projectId = $_POST['projectId'];
            $list = $db->objectList($currentUser->Id, $projectId);
            if($list){
                $result = $list;
            }else{
                _dbError();
            }
            break;

        case "findObject":
            $projectId = $_POST['projectId'];
            $barcode = $_POST['barcode'];
            $list = $db->findObject($projectId, $barcode);
            if($list !== false){
                $result = $list;
            }else{
                _dbError();
            }
            break;

        case 'addObject':
            $name = $_POST['name'];
            $barcode = $_POST['barcode'];
            $projectId = $_POST['projectId'];
            $userName = $_POST['userName'];
            $res = $db->addObject($name, $barcode, $projectId, $userName);
            if($res){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        case 'checkUserGranted':
            $objectId = $_POST['objectId'];
            $userId = $_POST['userId'];
            $res = $db->checkUserGranted($objectId, $userId);
            if($res !== false){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        case 'grantUser':
            $objectId = $_POST['objectId'];
            $userId = $_POST['userId'];
            $userName = $_POST['userName'];
            $checked = $_POST['checked'];
            $barcode = $_POST['barcode'];
            $res = $db->grantUser($objectId, $userId, $userName, $barcode, $checked);
            if($res !== false){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        case 'checkObject':
            $objectId = $_POST['objectId'];
            $userId = $_POST['userId'];
            $barcode = $_POST['barcode'];
            $res = $db->checkObject($objectId, $userId, $barcode);
            if($res !== false){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        case 'addFile':
            $objectId = $_POST['objectId'];
            $fName = $_POST['fName'];
            $fExt = $_POST['fExt'];
            $fSize = $_POST['fSize'];
            $descr = $_POST['descr'];
            $id = $db->addFile($objectId, $fName, $fExt, $descr, $fSize);
            if($id !== false){
                $result = $id;

                print_r($_FILES);

                if($_FILES['file']['tmp_name'] == ""){
                    header("HTTP/1.1 400 File upload error ".$_FILES['file']['error']);
                }

                $tmpFile = $_FILES['file']['tmp_name'];
                $new_file_name = "files/file_$id";
                $content = file_get_contents($tmpFile);
                if(!$content){
                    header("HTTP/1.1 400 File open error");
                }
    //            move_uploaded_file($tmpFile, $new_file_name);

    //            $content = base64_decode($_POST['data']);

                if(!$db->addFileData($id, $content)){
                    _dbError();
                }
            }else{
                _dbError();
            }
            break;

        case 'fileList':
            $objectId = $_POST['objectId'];
            $res = $db->fileList($objectId);
            if($res !== false){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        case 'fileData':
            $fileId = key_exists('fileId', $_POST) ? $_POST['fileId'] : $_GET['fileId'];
    //        $fileExt = key_exists('fExt', $_POST) ? $_POST['fExt'] : $_GET['fExt'];
            $fData = $db->fileData($fileId);
            if($fData !== false){
    //            $mimes = include('mime_types.php');
    //            $mime = $mimes[$fileExt];
    //            header("Content-Type: image/jpeg");

                echo $fData;
                exit();
            }else{
                _dbError();
            }
            break;

        case 'updateDescription':
            $object_id = $_POST['object_id'];
            $descr = $_POST['descr'];
            $res = $db->updateDescription($object_id, $currentUser->Id, $descr);
            if($res !== false){
                $result = $res;
            }else{
                _dbError();
            }
            break;

        default:
            header("HTTP/1.1 400 Bad Request");
    }
}catch(Exception $e){
    header("HTTP/1.1 500 $e");
}

echo json_encode($result);