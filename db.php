<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

class database{
    const db_server = '192.168.25.94';
    const db_name = 'barcode';
    const db_user = 'barcode';
    const db_pass = 'BarCode_Password_19';
    
    private $db;
    public $error = '';
    
    function __construct(){
        try{
            $this->db = pg_connect("host=".self::db_server." dbname=".self::db_name." user=".self::db_user." password=".self::db_pass);
        }catch(Exception  $e){
            $this->error = $e;
        }
    }
    
    function _query($sql){
        try{
            $lines = [];
            $res = pg_query($sql);
            if($res === false){
                $this->error = pg_errormessage($this->db);
                return false;
            }
            while($line = pg_fetch_object($res)){
                $lines[] = $line;
            }
            return $lines;
        }catch(Exception $e){
            $this->error = $e."; ".$sql;
            return false;
        }
    }
    
    function _upd_query($sql){
        try{
            $lines = [];
            if(pg_query($sql)){
                return true;
            }else{
                $this->error = "DB error";
                return false;
            }
        }catch(Exception $e){
            $this->error = $e;
            return false;
        }
    }
    
    function _setSession($userName){
        $sql = <<<END
            SET SESSION usrvar.name ='$userName';
END;
        try{
            return $this->_upd_query($sql);
        }catch(Exception $e){
            $this->error = e;
            return false;
        }
    }
    
    function checkUser($name, $pass){
        $sql = <<<END
            SELECT 
              barcode."Users"."Id", 
              barcode."Users"."Name", 
              "Description", 
              "Id_UserRoles", 
              "Id_Organizations", 
              "Document", 
              "Password",
              barcode."UserRoles"."Name" as "UserRole",
              barcode."UserRoles"."Id" as "UserRoleId"
            FROM 
              barcode."Users", 
              barcode."UserRoles"
            WHERE       barcode."Users"."Id_UserRoles"=barcode."UserRoles"."Id"
              and barcode."Users"."Name"='$name'
              and barcode."Users"."Password"='$pass';
END;
        
        $user = (object)[];
        $lines = $this->_query($sql);
//        print_r($name);
//        print_r($lines);
        if(count($lines)>0){
            $user = $lines[0];
            $user->Id = intval($user->Id);
            if(!$this->_setSession($user->Name)){
                return false;
            }
        }else{
            $user->Id = -1;
            $user->Name = "";
        }
    
        return $user;
    }
    
    function projectList(){
        $sql = <<<END
      SELECT 
        "Id", "Name", "Description"
      FROM 
        barcode."Project";
END;
        
        $lines = $this->_query($sql);
        foreach ($lines as $line) {
            $line->Id = intval($line->Id);
        }
        return $lines;
    }
    
    function objectList($userId, $projectId){
        $sql = <<<END
            SELECT 
              public."ZipEntities"."Id" as "Id_Obj", 
              "Id_Parent", 
              public."ZipEntities"."Name",
              public."ZipEntities"."Description", 
              "Id_ZipTypes", 
              "Id_Status", 
              "Count", 
              "Id_Dimentions", 
              "Id_Project", 
              public."ZipEntities"."Barcode",
              "Id_User",
              "IsChecked",
              barcode."CheckItem"."Barcode" as "BarCode_Found",
              "CheckedTime", 
              barcode."CheckItem"."Description" as "Found_Descr",
              barcode."Users"."Name" as "UserName"
            FROM 
              public."ZipEntities",
              barcode."CheckItem",
              barcode."Users"
            WHERE
            public."ZipEntities"."Id"=barcode."CheckItem"."Id_ZipEntities"
            and barcode."Users"."Id"=barcode."CheckItem"."Id_User"
            and "Id_Project"=$projectId
            and "Id_User"=$userId
END;
        $lines = $this->_query($sql);
        if($lines === false){
            return false;
        }
        foreach ($lines as $line) {
            $line->Id_Obj = intval($line->Id_Obj);
            $line->Id_Project = intval($line->Id_Project);
            $line->Id_User = intval($line->Id_User);
            $line->IsChecked = $line->IsChecked==='t';
        }
        return $lines;
    }
    
    function findObject($projectId, $barcode){
        $sql = <<<END
            SELECT * 
            FROM 
              public."ZipEntities"
            WHERE
              "Id_Project"= $projectId
              and "Barcode"= '$barcode'    
END;
        $lines = $this->_query($sql);
        foreach ($lines as $line) {
            $line->Id = intval($line->Id);
//            $line->IsChecked = $line->IsChecked === "t";
        }
        return $lines;
    }
    
    function addObject($name, $barcode, $projectId, $userName){
        if(!$this->_setSession($userName)){
            return false;
        }
        
        $sql = <<<END
            INSERT INTO
            public."ZipEntities"
            (
              "Id_Parent",
              "Name",
              "Barcode",  
              "Description",
              "Id_ZipTypes",
              "Id_Status",
              "Count",
              "Id_Dimentions",
              "Id_Project"
            )
            VALUES 
            (
              0,
              '$name',
              '$barcode',
              '',
              1,
              4,
              1,
              7,
              $projectId
            );
END;
        
        if(!$this->_upd_query($sql)){
            return false;
        }
        
        $sql = <<<END
            SELECT 
              last_value 
            FROM 
              public."ZipEntities_Id_seq";
END;
        
        $lines = $this->_query($sql);
        if(count($lines) == 0){
            $this->error = 'Add object error';
            return false;
        }
        
        return intval($lines[0]->last_value);
    }
    
    function checkUserGranted($objectId, $userId){
        $sql = <<<END
            SELECT 
              "Id", 
              "Id_ZipEntities", 
              "Id_User", 
              "IsChecked",    
              "Barcode", 
              "CheckedTime", 
              "Description"
            FROM 
              barcode."CheckItem"
            WHERE
              "Id_ZipEntities"=$objectId and "Id_User"=$userId;
END;
        
        $lines = $this->_query($sql);
        if($lines === false){
            return false;
        }
        return $lines;
    }
    
    function grantUser($objectId, $userId, $userName, $barcode, $checked){
        if(!$this->_setSession($userName)){
            return false;
        }
        
        $sql = <<<END
            INSERT INTO 
            barcode."CheckItem" 
            (
              "Id_ZipEntities",
              "Id_User",
              "IsChecked",
              "Barcode"
            )
            VALUES 
            (
              $objectId,
              $userId,
              $checked,
              '$barcode'
            );
END;
              
        return $this->_upd_query($sql);
    }
    
    function checkObject($objectId, $userId, $barcode){
        $sql = <<<END
            INSERT INTO 
            barcode."CheckItem" 
            (
              "Id_ZipEntities",
              "Id_User",
              "IsChecked",
              "Barcode"
            )
            VALUES 
            (
              $objectId,
              $userId,
              true,
              '$barcode'
            )
            ON CONFLICT ON CONSTRAINT "Id_Zip_Id_User"
            DO UPDATE SET 
              "IsChecked"= true,
              "Barcode"= '$barcode';
END;
              
        return $this->_upd_query($sql);
    }
    
    function addFile($objectId, $fName, $fExt, $descr, $fSize){
        $sql = <<<END
            INSERT INTO 
            public."Files"
            (
              "Id_ZipEntities", 
              "Name", 
              "Description", 
              "FileSize", 
              "Ext"
            )
            VALUES 
            (
              $objectId,
              '$fName',
              '$descr',
              '$fSize',
              '.$fExt'
            )
            RETURNING "Id";
END;
              
        $res = $this->_query($sql);
        if($res === false){
            return false;
        }
        if(count($res)==0){
            $this->error = 'Add file error';
            return false;
        }
        return $res[0]->Id;
    }
    
    function addFileData($fileId, $data){
        $sql = <<<END
            INSERT INTO 
            public."FilesData"
            (
              "Id_Files", 
              "Data"
            )
            VALUES 
            (
              $1,
              decode($2, 'base64')
            );
END;
        
        if(!pg_query_params($sql, [$fileId, base64_encode($data)])){
            $this->error = pg_errormessage($this->db);
            return false;
        }
        
        return true;
    }
    
    function fileList($objectId){
        $sql = <<<END
            Select 
                public."Files"."Id", 
                public."Files"."Name", 
                public."Files"."Ext",
                public."Files"."Description", 
                public."Files"."FileSize"
            from public."Files" 
            where public."Files"."Id_ZipEntities" = $objectId;
END;
        
        $res = $this->_query($sql);
        foreach ($res as $line) {
            $line->Id = intval($line->Id);
        }
        if($res !== false){
            return $res;
        }else{
            return false;
        }
    }
    
    function fileData($fileId){
        $cache_name = "files/file_$fileId";
        if(file_exists($cache_name)){
            return $cache_name;
        }
        
        $sql = <<<END
            Select encode("Data", 'base64') as "Data" from public."FilesData" where "Id_Files"=$fileId;    
END;
        
        $res = $this->_query($sql);
        if($res === false){
            $this->error = "No file data";
            return false;
        }
        
        if(count($res)==0){
            $this->error = "Empty file list";
            return false;
        }
        
        $fData = base64_decode($res[0]->Data);
//        $tmpName = "/tmp/tmp_$fileId.jpg";
//        if(!file_put_contents($tmpName, $fData)){
//            $this->error = "Save File error $tmpName";
//            return false;
//        }
        
        return $fData;
    }
    
    function updateDescription($object_id, $user_id, $descr){
        $sql = <<<END
            UPDATE 
              barcode."CheckItem"
            SET 
              "Description"='$descr'
            WHERE 
              "Id_ZipEntities"=$object_id 
               and  "Id_User"=$user_id;
END;
        
        $res = $this->_upd_query($sql);
        return $res;
   }
}